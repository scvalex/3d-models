module tooth() {
    difference() {
        union() {
            cube([14, 14, 17]);
            translate([7, 14, 17]) {
                rotate(90, [1, 0, 0]) {
                    cylinder(14, 6, 6, $fn=40);
                }
            }
        }
        translate([7, 14, 17]) {
            rotate(90, [1, 0, 0]) {
                translate([0, 0, -1]) {
                    cylinder(16, 3.2, 3.2, $fn=40);
                }
            }
        }
    }
}

difference() {
    union() {
        translate([-25, -12.5, 13]) {
            difference() {
                cube([5, 25, 34]);
                translate([-1, 0, 26]) {
                    rotate(45, [1, 0, 0]) {
                        cube([7, 12, 12]);
                    }
                }
                translate([-1, 25, 26]) {
                    rotate(45, [1, 0, 0]) {
                        cube([7, 12, 12]);
                    }
                }
            }
            cube([5, 25, 14]);
            rotate(90, [0, 1, 0]) {
                translate([-7, 0, 0]) {
                    cylinder(5, 7, 7, $fn=6);
                }
                translate([-7, 25, 0]) {
                    cylinder(5, 7, 7, $fn=6);
                }
            }
        }
        translate([-25, -7, 13]) {
            rotate(270, [0, 1, 0]) {
                tooth();
            }
        }
    }
    translate([-30, 0, 36]) {
        rotate(90, [0, 1, 0]) {
            cylinder(20, 3.2, 3.2, $fn=40);
        }
    }
    rotate(30, [0, 0, 1]) {
        translate([0, 0, -2]) {
            cylinder(44, 9, 9, $fn=6);
        }
    }
} 