module onePipe() {
    union() {
        translate([17, 0, -2]) {
            cylinder(14, 8, 8, $fn=40); 
        }
        translate([26, -10, -2]) {
            cube([30, 20, 14]);
        }
        translate([22, 0, 5]) {
            rotate(90, [0, 1, 0]) {
                cylinder(20, 3.2, 3.2, $fn=40);
            }
        }
        translate([22, 0, 5]) {
            rotate(270, [0, 1, 0]) {
                cylinder(20, 3.2, 3.2, $fn=40);
            }
        }
    }    
}
 
difference() {
    cylinder(10, 40, 40, $fn=3);
    union() {
        onePipe();
    }
    rotate(120, [0, 0, 1]) {
        onePipe();
    }
    rotate(240, [0, 0, 1]) {
        onePipe();
    }
    rotate(30, [0, 0, 1]) {
        translate([0, 0, -2]) {
            cylinder(14, 9, 9, $fn=6);
        }
    }
} 