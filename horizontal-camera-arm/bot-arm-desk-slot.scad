module onePipe() {
    union() {
        translate([17, 0, -2]) {
            cylinder(44, 8, 8, $fn=40); 
        }
        translate([32, -10, -2]) {
            cube([30, 20, 44]);
        }
        translate([23, -5, -2]) {
            cube([5, 10, 44]);
        }
        translate([22, 0, 20]) {
            rotate(90, [0, 1, 0]) {
                cylinder(20, 3.2, 3.2, $fn=40);
            }
        }
    }    
}
 
difference() {
    union() {
        cylinder(40, 40, 40, $fn=3);
        translate([-28, -2.4, 0]) {
            difference() {
                translate([0, -18.6, 0]) {
                    cube([8, 42, 25]);
                }
                translate([-1, 20, -5]) {
                    rotate(45, [0, 0, 1]) {
                        cube([10, 10, 35]);
                    }
                }
                translate([0, -30, -5]) {
                    rotate(45, [0, 0, 1]) {
                        cube([10, 10, 35]);
                    }
                }
            }
            translate([0, 0, 25]) {
                cube([8, 4.8, 55]);
            }
            translate([0, -7.6, 25]) {
                cube([2, 20, 55]);
            }
        }
    }
    union() {
        onePipe();
    }
    rotate(120, [0, 0, 1]) {
        onePipe();
    }
    rotate(240, [0, 0, 1]) {
        onePipe();
    }
    rotate(30, [0, 0, 1]) {
        translate([0, 0, -2]) {
            cylinder(44, 9, 9, $fn=6);
        }
    }
} 