3d models
=========

Render these with OpenSCAD: https://openscad.org/

## Useful links

- Cheatsheet: https://openscad.org/cheatsheet/
- User manual: https://en.wikibooks.org/wiki/OpenSCAD_User_Manual
- Commands: https://en.wikibooks.org/wiki/OpenSCAD_User_Manual/Command_Glossary
- 3D primitives: https://en.wikibooks.org/wiki/OpenSCAD_User_Manual/Primitive_Solids
- User functions: https://en.wikibooks.org/wiki/OpenSCAD_User_Manual/User-Defined_Functions_and_Modules
