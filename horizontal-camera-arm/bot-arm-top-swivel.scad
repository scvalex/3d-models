module tooth() {
    difference() {
        union() {
            translate([0, 1, 0]) {
                cube([14, 4, 17]);
            }
            translate([7, 5, 17]) {
                rotate(90, [1, 0, 0]) {
                    cylinder(4, 6, 6, $fn=40);
                }
            }
        }
        translate([7, 5, 17]) {
            rotate(90, [1, 0, 0]) {
                translate([0, 0, -1]) {
                    cylinder(7, 3.2, 3.2, $fn=40);
                }
            }
        }
    }
}

union() {
    difference() {
        translate([0, 5, 0]) {
            union() {
                cube([14, 25, 5]);
                translate([7, 0, 0]) {
                    cylinder(5, 7, 7, $fn=6);
                }
                translate([7, 25, 0]) {
                    cylinder(5, 7, 7, $fn=6);
                }
                translate([-5.5, 5.5, 0]) {
                    cube([25, 14, 5]);
                }
                translate([-5.5, 12.5, 0]) {
                    rotate(30, [0, 0, 1]) {
                        cylinder(5, 7, 7, $fn=6);
                    }
                }
                translate([19.5, 12.5, 0]) {
                    rotate(30, [0, 0, 1]) {
                        cylinder(5, 7, 7, $fn=6);
                    }
                }
                translate([10.5, 0, 5]) {
                    rotate(270, [0, 1, 0]) {
                        cylinder(7, 5, 5, $fn=3);
                    }
                }
                translate([10.5, 25, 5]) {
                    rotate(270, [0, 1, 0]) {
                        cylinder(7, 5, 5, $fn=3);
                    }
                }
            }
        }
        translate([7, 17.5, -1]) {
            cylinder(10, 3.2, 3.2, $fn=40);
        }
    }
    translate([14, 10, 5]) {
        rotate(180, [0, 0, 1]) {
            tooth();
        }
    }
    translate([0, 25, 5]) {
        tooth();
    }
}