module onePipe() {
    union() {
        translate([17, 0, -2]) {
            cylinder(44, 8, 8, $fn=40); 
        }
        translate([32, -10, -2]) {
            cube([30, 20, 44]);
        }
        translate([23, -5, -2]) {
            cube([5, 10, 44]);
        }
        translate([22, 0, 20]) {
            rotate(90, [0, 1, 0]) {
                cylinder(20, 3.2, 3.2, $fn=40);
            }
        }
    }    
}
 
module tooth() {
    difference() {
        union() {
            cube([14, 14, 7]);
            translate([7, 14, 7]) {
                rotate(90, [1, 0, 0]) {
                    cylinder(14, 6, 6, $fn=40);
                }
            }
        }
        translate([7, 14, 7]) {
            rotate(90, [1, 0, 0]) {
                translate([0, 0, -1]) {
                    cylinder(16, 3.2, 3.2, $fn=40);
                }
            }
        }
    }
}

difference() {
    union() {
        cylinder(40, 40, 40, $fn=3);
        translate([-25, -12.5, 13]) {
            cube([5, 25, 14]);
            rotate(90, [0, 1, 0]) {
                translate([-7, 0, 0]) {
                    cylinder(5, 7, 7, $fn=6);
                }
                translate([-7, 25, 0]) {
                    cylinder(5, 7, 7, $fn=6);
                }
            }
        }
        translate([-25, -7, 13]) {
            rotate(270, [0, 1, 0]) {
                tooth();
            }
        }
    }
    union() {
        onePipe();
    }
    rotate(120, [0, 0, 1]) {
        onePipe();
    }
    rotate(240, [0, 0, 1]) {
        onePipe();
    }
    rotate(30, [0, 0, 1]) {
        translate([0, 0, -2]) {
            cylinder(44, 9, 9, $fn=6);
        }
    }
} 