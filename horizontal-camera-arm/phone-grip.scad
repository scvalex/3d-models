union() {
    difference() {
        union() {
            translate([0, 0, -15]) {
                cube([82, 15, 19]);
            }
            translate([33.5, 0, 0]) {
                cube([15, 15, 10]);
            }
        }
        translate([35, 1.5, -0.5]) { // screw head space
            cube([12, 12, 4.5]);
        }
        translate([41, 7.5, -1]) { // hole
            cylinder(100, 3.2, 3.2, $fn=40);
        }
        translate([3, -100, -12]) {
            cube([76, 200, 12]);  // phone
        }
        translate([8, -100, -16]) { //below phone
            cube([66, 200, 12]);
        }
    }
    
}