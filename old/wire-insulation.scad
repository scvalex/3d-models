height = 20;
side = 10;
height2 = height - 1.5;
side2 = side - 1.5;
hole_r = (side2 / 2) - 0.5;

translate([0, 0, side]) {
    rotate([0, 90, 0]) {
        difference() {
            cube([side, side, height]);
            translate([side / 2, side / 2, height]) {
                cylinder(h = side, r = hole_r, center = true);
            }
            translate([0, 0.5, height - 1.5]) {
                cube([side / 2, side - 1, 2]);
            }
            translate([0, 0.5, 0.5]) {
                cube([side - 1, side - 1, height - 2]);
            }
        }
    }
}

translate([0, side+2, -1.5]) {
    rotate([0, 90, 0]) {
        translate([-10, 0.75, 0]) {
            difference() {
                cube([side2, side2, height2]);
                translate([side2 / 2, side2 / 2, height2]) {
                    cylinder(h = side2, r = hole_r, center = true);
                }
                translate([0, 0.5, height2 - 2]) {
                    cube([side2 / 2, side2 - 1, 2]);
                }
                translate([0, 0.5, 1]) {
                    cube([side2 - 0.6, side2 - 1, height2 - 2]);
                }
            }
        }
    }
}

//leg_height = 40;
//leg_length = 19;
//#leg_width = 20;
//#lip_height = 10;
//#small_lip_height = 7;
//#small_lip_depth = 2;
//#foot_radius = 2;
//#grid_space_small = 1.5;
//#grid_space_large = 13;
//#union() {
//#    difference() {
//#        cube([leg_width, leg_length, leg_height]);
//#        translate([small_lip_depth, small_lip_depth, leg_height - lip_height]) {
//#            cube([leg_width, leg_length, leg_height]);
//#        }
//#        translate([0, small_lip_depth, leg_height - lip_height + small_lip_height]) {
//#            cube([small_lip_depth, leg_length - small_lip_depth, lip_height - small_lip_height]);
//#        }
//#    }
//#    translate([foot_radius,3,-2.5]) {
//#        for (i=[0:2]) {
//#            for (j=[0:1]) {
//#                translate([(2*grid_space_small + 2*foot_radius)*i, grid_space_large*j, 0]) {
//#                    cylinder(h = 5, r = foot_radius, center = true);
//#                }
//#            }
//#        }
//#    }
//#}